package com.mentorcliq.startup.metadata;

import com.mentorcliq.common.datatype.annotations.Matchable;
import com.mentorcliq.entity.MatcherMetadata;
import com.mentorcliq.repository.MatcherMetadataRepository;
import com.mentorcliq.startup.ApplicationSetup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author William Arustamyan
 */

@Component
public class MatcherMetadataSetup implements ApplicationSetup {

    private final MatcherMetadataRepository repository;

    @Value("${matcher.entity.package}")
    private String lookupPackage;


    @Autowired
    public MatcherMetadataSetup(final MatcherMetadataRepository repository) {
        this.repository = repository;
    }


    @Override
    public void initialize() {
        try {
            final Set<Class<?>> matchables = matchableEntices();
            final List<MatcherMetadata> metadatas = new ArrayList<>(matchables.size());
            matchables.forEach(it -> {
                final Annotation found = AnnotationUtils.findAnnotation(it, Matchable.class);
                final Object type = AnnotationUtils.getValue(found, "type");
                if (type != null) {
                    metadatas.add(new MatcherMetadata(type.toString()));
                }
            });
            if (!matchables.isEmpty()) {
                repository.saveAll(metadatas);
            }
        } catch (Exception e) {
            throw new IllegalStateException("Application startup fails: unable to identify matchable entity's");
        }
    }

    private Set<Class<?>> matchableEntices() throws Exception {
        final ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
        final MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);

        final Set<Class<?>> candidates = new HashSet<>();
        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX +
                resolveBasePackage(lookupPackage) + "/" + "**/*.class";
        final Resource[] resources = resourcePatternResolver.getResources(packageSearchPath);
        for (Resource resource : resources) {
            if (resource.isReadable()) {
                MetadataReader metadataReader = metadataReaderFactory.getMetadataReader(resource);
                if (isCandidate(metadataReader)) {
                    candidates.add(Class.forName(metadataReader.getClassMetadata().getClassName()));
                }
            }
        }
        return candidates;
    }

    private String resolveBasePackage(String basePackage) {
        return ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(basePackage));
    }

    private boolean isCandidate(MetadataReader metadataReader) throws ClassNotFoundException {
        Class<?> c = Class.forName(metadataReader.getClassMetadata().getClassName());
        return c.getAnnotation(Matchable.class) != null;
    }
}
