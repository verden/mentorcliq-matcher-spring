package com.mentorcliq.startup.resourcereader;

import com.mentorcliq.entity.Matcher;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

/**
 * @author William Arustamyan
 */


@Component
public class EmployeeMatcherResourceReader implements MatcherReader<Matcher> {


    @Override
    public Set<Matcher> readMatchers() {
        Resource resource = new ClassPathResource("com/matcher/default-employee-matchers.properties");
        Properties property;
        try {
            property = PropertiesLoaderUtils.loadProperties(resource);
            final String matcherType = property.getProperty("matcher.type");
            int count = Integer.parseInt(property.getProperty("matcher.count"));
            final Set<Matcher> matchers = new HashSet<>(count);
            for (int i = 0; i < count; ++i) {
                final Matcher matcher = new Matcher(matcherType);
                matcher.setName(property.getProperty("matcher." + i + ".name"));
                matcher.setDescription(property.getProperty("matcher." + i + ".description"));
                matcher.setExpression(property.getProperty("matcher." + i + ".expression"));
                matcher.setTrueCaseValue(property.getProperty("matcher." + i + ".true_case_val"));
                matcher.setFalseCaseValue(property.getProperty("matcher." + i + ".false_case_val"));
                matchers.add(matcher);
            }
            return matchers;
        } catch (IOException e) {
            throw new IllegalStateException("Application startup fail : unable to load default employee matcher properties");
        }
    }
}
