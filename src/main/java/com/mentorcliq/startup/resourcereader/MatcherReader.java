package com.mentorcliq.startup.resourcereader;

import java.util.Set;

/**
 * @author William Arustamyan
 */

public interface MatcherReader<T> {

    Set<T> readMatchers();

}
