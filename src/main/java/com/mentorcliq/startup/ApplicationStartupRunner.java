package com.mentorcliq.startup;

import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author William Arustamyan
 */

@Service
public class ApplicationStartupRunner implements StartupRunner {

    private final Set<? extends ApplicationSetup> startupSetup;

    public ApplicationStartupRunner(final Set<ApplicationSetup> setups) {
        this.startupSetup = new HashSet<>(setups);
    }


    @Override
    public void onStart() {
        this.startupSetup.forEach(ApplicationSetup::initialize);
    }

    @Override
    public void onStop() {
        throw new UnsupportedOperationException("Currently not implemented ...");
    }
}
