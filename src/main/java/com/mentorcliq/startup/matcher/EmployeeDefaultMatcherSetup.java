package com.mentorcliq.startup.matcher;

import com.mentorcliq.entity.Matcher;
import com.mentorcliq.repository.MatcherRepository;
import com.mentorcliq.startup.ApplicationSetup;
import com.mentorcliq.startup.resourcereader.MatcherReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author William Arustamyan
 */

@Component
public class EmployeeDefaultMatcherSetup implements ApplicationSetup {

    private final MatcherRepository matcherRepository;

    private final MatcherReader<Matcher> employeeMatcherResourceReader;

    @Autowired
    public EmployeeDefaultMatcherSetup(final MatcherRepository matcherRepository, final MatcherReader<Matcher> reader) {
        this.matcherRepository = matcherRepository;
        this.employeeMatcherResourceReader = reader;
    }


    @Override
    public void initialize() {
        final Set<Matcher> matchers = this.employeeMatcherResourceReader.readMatchers();
        matcherRepository.saveAll(matchers);
    }
}
