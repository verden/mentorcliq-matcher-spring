package com.mentorcliq.startup;

/**
 * @author William Arustamyan
 */

public interface ApplicationSetup {

    void initialize();
}
