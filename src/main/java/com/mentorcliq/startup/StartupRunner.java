package com.mentorcliq.startup;

/**
 * @author William Arustamyan
 */

public interface StartupRunner {

    void onStart();

    void onStop();
}
