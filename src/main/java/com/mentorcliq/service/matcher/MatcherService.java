package com.mentorcliq.service.matcher;

import com.mentorcliq.common.datatype.MatcherAddRequest;
import com.mentorcliq.common.datatype.MatcherEditRequest;
import com.mentorcliq.entity.Matcher;
import com.mentorcliq.service.AddSupported;
import com.mentorcliq.service.DeleteSupported;
import com.mentorcliq.service.EditSupported;
import com.mentorcliq.service.GetSupported;
import com.mentorcliq.service.ListSupported;

import java.util.List;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

public interface MatcherService extends
        AddSupported<List<Matcher>, MatcherAddRequest>,
        EditSupported<List<Matcher>, MatcherEditRequest>,
        ListSupported<Matcher>,
        DeleteSupported<List<Matcher>, UUID>,
        GetSupported<Matcher, UUID> {
}
