package com.mentorcliq.service.matcher;

import com.mentorcliq.common.datatype.MatcherAddRequest;
import com.mentorcliq.common.datatype.MatcherEditRequest;
import com.mentorcliq.common.datatype.exception.DomainNotFoundException;
import com.mentorcliq.entity.Matcher;
import com.mentorcliq.repository.MatcherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

@Service
public class MatcherServiceImpl implements MatcherService {

    private MatcherRepository matcherRepository;


    @Override
    public Matcher get(final UUID uuid) {
        final Optional<Matcher> found = matcherRepository.findById(uuid);
        if (found.isEmpty()) {
            throw new DomainNotFoundException(String.format("Matcher with id: %s not found", uuid.toString()));
        }
        return found.get();
    }

    @Override
    public List<Matcher> list() {
        return matcherRepository.findAll();
    }

    @Autowired
    public void setMatcherRepository(MatcherRepository repository) {
        this.matcherRepository = repository;
    }

    @Override
    public List<Matcher> add(final MatcherAddRequest addRequest) {
        final Matcher instance = new Matcher(addRequest.getMatcherType());
        instance.setName(addRequest.getName());
        instance.setDescription(addRequest.getDescription());
        instance.setExpression(addRequest.getExpression());
        instance.setTrueCaseValue(addRequest.getTrueCaseValue());
        instance.setFalseCaseValue(addRequest.getFalseCaseValue());
        matcherRepository.save(instance);
        return matcherRepository.findAll();
    }

    @Override
    public List<Matcher> delete(final UUID uuid) {
        matcherRepository.deleteById(uuid);
        return this.list();
    }

    @Override
    public List<Matcher> edit(final MatcherEditRequest editRequest) {
        Optional<Matcher> found = matcherRepository.findById(UUID.fromString(editRequest.getGuid()));
        if (found.isPresent()) {
            final Matcher matcher = found.get();
            matcher.setName(editRequest.getName());
            matcher.setDescription(editRequest.getDescription());
            matcher.setExpression(editRequest.getExpression());
            matcher.setTrueCaseValue(editRequest.getTrueCaseValue());
            matcher.setFalseCaseValue(editRequest.getFalseCaseValue());
            matcherRepository.save(matcher);
        }
        return this.list();
    }
}
