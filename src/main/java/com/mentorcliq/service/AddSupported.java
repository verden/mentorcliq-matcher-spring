package com.mentorcliq.service;

/**
 * @author William Arustamyan
 */

public interface AddSupported<D, A> {

    D add(A addRequest);
}
