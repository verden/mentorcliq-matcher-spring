package com.mentorcliq.service;

/**
 * @author William Arustamyan
 */

public interface EditSupported<D, E> {

    D edit(E editRequest);
}
