package com.mentorcliq.service;

import java.util.List;

/**
 * @author William Arustamyan
 */

public interface ListSupported<L> {

    List<L> list();
}
