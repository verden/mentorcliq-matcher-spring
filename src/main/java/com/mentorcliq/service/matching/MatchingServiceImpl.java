package com.mentorcliq.service.matching;

import com.mentorcliq.common.datatype.MatchRequest;
import com.mentorcliq.common.datatype.MatchingResult;
import com.mentorcliq.common.datatype.MatchingResultResponse;
import com.mentorcliq.common.datatype.Pair;
import com.mentorcliq.common.datatype.PairResponse;
import com.mentorcliq.common.datatype.RecommendationResponse;
import com.mentorcliq.common.datatype.Score;
import com.mentorcliq.entity.Employee;
import com.mentorcliq.entity.Matcher;
import com.mentorcliq.service.employee.EmployeeService;
import com.mentorcliq.service.matcher.MatcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

@Service
public class MatchingServiceImpl implements MatchingService {

    //this can be resolved from message file
    //currently can't do this sorry :)
    private static final String default_recommendation_message = "%s with %s = ";

    private static final String score_message = "Average score = %s";

    private EmployeeService employeeService;

    private MatcherService matcherService;

    private MatchingEvaluationService evaluationService;

    @Override
    public MatchingResultResponse doMatch(final MatchRequest matchRequest) {
        if (matchRequest.getFirstEmployeeGuid().equals(matchRequest.getSecondEmployeeGuid())) {
            return new MatchingResultResponse("Same employee matching is always 100% :D");
        }
        final Employee firstEmployee = employeeService.get(UUID.fromString(matchRequest.getFirstEmployeeGuid()));
        final Employee secondEmployee = employeeService.get(UUID.fromString(matchRequest.getSecondEmployeeGuid()));
        if (matchRequest.allSelected()) {
            final MatchingResult result = this.matchMultiple(firstEmployee, secondEmployee, matcherService.list());
            return new MatchingResultResponse(result, "Average matching score between employee %s and %s is: ");
        }
        final Matcher matcher = matcherService.get(UUID.fromString(matchRequest.getMatcherGuid()));
        final Score score = evaluationService.evaluate(firstEmployee, secondEmployee, matcher);
        return new MatchingResultResponse(new MatchingResult(firstEmployee, secondEmployee, score), "Matching result between employees %s and %s is: ");
    }

    private MatchingResult matchMultiple(final Employee firstEmployee, final Employee secondEmployee, final List<Matcher> matchers) {
        int summary = 0;
        for (Matcher matcher : matchers) {
            summary = summary + evaluationService.evaluate(firstEmployee, secondEmployee, matcher).asInteger();
        }
        return new MatchingResult(firstEmployee, secondEmployee, new Score(summary));
    }

    @Override
    public RecommendationResponse recommendations() {
        final List<Employee> employees = employeeService.list();
        final List<Matcher> matchers = matcherService.list();
        final List<MatchingResult> matchingResults = processMatching(employees, matchers);
        final List<Pair> pairs = this.makePairs(matchingResults);
        return this.buildResponse(pairs);
    }

    private List<MatchingResult> processMatching(final List<Employee> employees, final List<Matcher> matchers) {
        int currentIndex = 0;
        final List<MatchingResult> matchingResults = new ArrayList<>((employees.size() * (employees.size() - 1)) / 2 + 1);
        while (currentIndex < employees.size()) {
            final Employee currentEmployee = employees.get(currentIndex);
            for (int i = currentIndex + 1; i < employees.size(); i++) {
                matchingResults.add(this.matchMultiple(currentEmployee, employees.get(i), matchers));
            }
            ++currentIndex;
        }
        matchingResults.sort((m1, m2) -> (-1) * Integer.compare(m1.getScore().asInteger(), m2.getScore().asInteger()));
        return matchingResults;
    }

    private List<Pair> makePairs(final List<MatchingResult> matchingResults) {
        final List<Pair> pairs = new ArrayList<>(matchingResults.size() / 2 + 1);
        for (int i = 0; i < matchingResults.size() && i + 1 != matchingResults.size(); i = i + 2) {
            pairs.add(new Pair(matchingResults.get(i), matchingResults.get(i + 1)));
        }
        if (matchingResults.size() % 2 != 0) {
            pairs.add(new Pair(matchingResults.get(matchingResults.size() - 1), null));
        }
        return pairs;
    }

    private RecommendationResponse buildResponse(final List<Pair> pairs) {
        final List<PairResponse> responses = new ArrayList<>(pairs.size());
        for (final Pair pair : pairs) {
            final PairResponse response = new PairResponse();
            String firstMessage = String.format(default_recommendation_message,
                    pair.getLeft().getFirstEmployee().getName(), pair.getLeft().getSecondEmployee().getName()
            ).concat(pair.getLeft().getScore().scoreValue());
            if (pair.getRight() != null) {
                String secondMessage = String.format(default_recommendation_message,
                        pair.getRight().getFirstEmployee().getName(), pair.getRight().getSecondEmployee().getName()
                ).concat(pair.getRight().getScore().scoreValue());
                double avgScore = (double) (pair.getLeft().getScore().asInteger() + pair.getRight().getScore().asInteger()) / 2;
                response.setAvgScore(avgScore);
                response.setFirstMessage(firstMessage);
                response.setSecondMessage(secondMessage);
                response.setScoreMessage(String.format(score_message, avgScore));
            } else {
                response.setFirstMessage(firstMessage);
                response.setAvgScore(pair.getLeft().getScore().asDouble());
                response.setScoreMessage(String.format(score_message, pair.getLeft().getScore().asDouble()));
            }
            responses.add(response);
        }
        return new RecommendationResponse(responses);
    }


    @Autowired
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Autowired
    public void setMatcherService(MatcherService matcherService) {
        this.matcherService = matcherService;
    }

    @Autowired
    public void setEvaluationService(MatchingEvaluationService evaluationService) {
        this.evaluationService = evaluationService;
    }
}
