package com.mentorcliq.service.matching;

import com.mentorcliq.common.datatype.MatchRequest;
import com.mentorcliq.common.datatype.MatchingResultResponse;
import com.mentorcliq.common.datatype.RecommendationResponse;

/**
 * @author William Arustamyan
 */

public interface MatchingService {

    MatchingResultResponse doMatch(MatchRequest matchRequest);

    RecommendationResponse recommendations();
}
