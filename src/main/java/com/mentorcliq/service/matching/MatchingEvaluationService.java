package com.mentorcliq.service.matching;

import com.mentorcliq.common.datatype.Score;
import com.mentorcliq.entity.Employee;
import com.mentorcliq.entity.Matcher;
import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author William Arustamyan
 */

@Component
public class MatchingEvaluationService {

    private static final JexlEngine engine;

    static {
        engine = new JexlEngine();
        engine.setFunctions(Map.of("math", Math.class));
    }

    public Score evaluate(final Employee firstEmployee, final Employee secondEmployee, final Matcher matcher) {
        final Expression expression = engine.createExpression(matcher.getExpression());
        final JexlContext context = new MapContext();
        context.set("firstEmployee", firstEmployee);
        context.set("secondEmployee", secondEmployee);
        context.set("trueCaseVal", matcher.getTrueCaseValue());
        context.set("falseCaseVal", matcher.getFalseCaseValue());
        return new Score(expression.evaluate(context));
    }
}
