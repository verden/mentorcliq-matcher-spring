package com.mentorcliq.service;

/**
 * @author William Arustamyan
 */

public interface GetSupported<D, ID> {

    D get(ID id);
}
