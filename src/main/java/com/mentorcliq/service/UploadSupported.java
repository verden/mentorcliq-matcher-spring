package com.mentorcliq.service;

import java.io.InputStream;

/**
 * @author William Arustamyan
 */

public interface UploadSupported<D, E extends InputStream> {

    D upload(E stream);
}
