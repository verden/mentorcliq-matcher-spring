package com.mentorcliq.service.employee;

import com.mentorcliq.common.datatype.exception.DomainNotFoundException;
import com.mentorcliq.entity.Employee;
import com.mentorcliq.parser.UploadFileParser;
import com.mentorcliq.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeRepository employeeRepository;

    private UploadFileParser<List<Employee>> employeeFileParser;

    @Override
    public Employee get(final UUID uuid) {
        final Optional<Employee> found = employeeRepository.findById(uuid);
        if (found.isEmpty()) {
            throw new DomainNotFoundException(String.format("Employee with id: %s not found", uuid.toString()));
        }
        return found.get();
    }


    @Override
    public List<Employee> upload(final InputStream stream) {
        final List<Employee> employees = this.employeeFileParser.parse(stream);
        employeeRepository.saveAll(employees);
        return this.list();
    }

    @Override
    public List<Employee> list() {
        return employeeRepository.findAll();
    }

    @Autowired
    public void setEmployeeRepository(EmployeeRepository repository) {
        this.employeeRepository = repository;
    }

    @Autowired
    public void setEmployeeFileParser(UploadFileParser<List<Employee>> employeeFileParser) {
        this.employeeFileParser = employeeFileParser;
    }
}
