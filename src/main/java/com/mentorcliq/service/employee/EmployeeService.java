package com.mentorcliq.service.employee;

import com.mentorcliq.entity.Employee;
import com.mentorcliq.service.GetSupported;
import com.mentorcliq.service.ListSupported;
import com.mentorcliq.service.UploadSupported;

import java.io.InputStream;
import java.util.List;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

public interface EmployeeService extends UploadSupported<List<Employee>, InputStream>,
        ListSupported<Employee>, GetSupported<Employee, UUID> {
}
