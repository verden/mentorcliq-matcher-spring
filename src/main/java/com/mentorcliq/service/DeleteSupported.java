package com.mentorcliq.service;

/**
 * @author William Arustamyan
 */

public interface DeleteSupported<R, ID> {

    R delete(ID id);
}
