package com.mentorcliq.validation;

import com.mentorcliq.common.datatype.validation.Validatable;
import com.mentorcliq.common.datatype.validation.ValidationResult;

/**
 * @author William Arustamyan
 */

public interface ValidationService<R extends ValidationResult, I extends Validatable> {

    R validate(I instance);
}
