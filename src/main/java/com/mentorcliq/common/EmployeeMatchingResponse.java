package com.mentorcliq.common;

import java.util.UUID;

/**
 * @author William Arustamyan
 */

public final class EmployeeMatchingResponse {

    private UUID guid;

    private String name;

    public EmployeeMatchingResponse(UUID guid, String name) {
        this.guid = guid;
        this.name = name;
    }

    public EmployeeMatchingResponse() {
    }

    public UUID getGuid() {
        return guid;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
