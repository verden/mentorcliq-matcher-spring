package com.mentorcliq.common.datatype;

import com.mentorcliq.entity.Employee;

/**
 * @author William Arustamyan
 */

public final class MatchingResult {

    private final Employee firstEmployee;

    private final Employee secondEmployee;

    private final Score score;

    public MatchingResult(final Employee firstEmployee, final Employee secondEmployee, final Score score) {
        this.firstEmployee = firstEmployee;
        this.secondEmployee = secondEmployee;
        this.score = score;
    }


    public Employee getFirstEmployee() {
        return firstEmployee;
    }

    public Employee getSecondEmployee() {
        return secondEmployee;
    }

    public Score getScore() {
        return score;
    }
}
