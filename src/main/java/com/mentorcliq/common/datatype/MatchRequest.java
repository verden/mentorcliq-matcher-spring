package com.mentorcliq.common.datatype;

/**
 * @author William Arustamyan
 */

public final class MatchRequest {


    private String firstEmployeeGuid;

    private String secondEmployeeGuid;

    private String matcherGuid;

    public MatchRequest() {
    }

    public String getFirstEmployeeGuid() {
        return firstEmployeeGuid;
    }

    public void setFirstEmployeeGuid(String firstEmployeeGuid) {
        this.firstEmployeeGuid = firstEmployeeGuid;
    }

    public String getSecondEmployeeGuid() {
        return secondEmployeeGuid;
    }

    public void setSecondEmployeeGuid(String secondEmployeeGuid) {
        this.secondEmployeeGuid = secondEmployeeGuid;
    }

    public String getMatcherGuid() {
        return matcherGuid;
    }

    public void setMatcherGuid(String matcherGuid) {
        this.matcherGuid = matcherGuid;
    }

    public boolean allSelected() {
        return this.matcherGuid.equals("*");
    }
}
