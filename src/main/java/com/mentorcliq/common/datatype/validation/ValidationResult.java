package com.mentorcliq.common.datatype.validation;

/**
 * @author William Arustamyan
 */

public interface ValidationResult {
    String message();
}
