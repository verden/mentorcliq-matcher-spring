package com.mentorcliq.common.datatype.validation;

import java.util.Queue;

/**
 * @author William Arustamyan
 */

public class Rules {

    private final Queue<Rule> rules;

    public Rules(final Queue<Rule> rules) {
        this.rules = rules;
    }
}
