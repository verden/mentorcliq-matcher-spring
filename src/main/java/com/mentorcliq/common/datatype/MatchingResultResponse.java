package com.mentorcliq.common.datatype;

/**
 * @author William Arustamyan
 */

public class MatchingResultResponse {

    private final String message;

    public MatchingResultResponse(final MatchingResult matchingResult, final String messageSuf) {
        final String firstEmployeeName = matchingResult.getFirstEmployee().getName();
        final String secondEmployeeName = matchingResult.getSecondEmployee().getName();
        final Score score = matchingResult.getScore();
        this.message = String.format(messageSuf, firstEmployeeName, secondEmployeeName).concat(score.asPercentage());
    }

    public MatchingResultResponse(final String singleMessage) {
        this.message = singleMessage;
    }

    public String getMessage() {
        return message;
    }
}
