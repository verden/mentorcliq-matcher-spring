package com.mentorcliq.common.datatype;

/**
 * @author William Arustamyan
 */

public final class Pair {

    private MatchingResult left;
    private MatchingResult right;

    public Pair(final MatchingResult left, final MatchingResult right) {
        this.left = left;
        this.right = right;
    }

    public Pair() {
    }

    public void setLeft(MatchingResult left) {
        this.left = left;
    }

    public void setRight(MatchingResult right) {
        this.right = right;
    }

    public MatchingResult getLeft() {
        return this.left;
    }

    public MatchingResult getRight() {
        return this.right;
    }

    public void clean() {
        this.left = null;
        this.right = null;
    }

    public boolean isComplete() {
        return this.left != null && this.right != null;
    }
}
