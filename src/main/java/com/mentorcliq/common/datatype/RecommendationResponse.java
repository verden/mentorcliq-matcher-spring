package com.mentorcliq.common.datatype;

import java.util.List;

/**
 * @author William Arustamyan
 */

public final class RecommendationResponse {

    private List<PairResponse> items;

    public RecommendationResponse() {

    }

    public RecommendationResponse(final List<PairResponse> items) {
        this.items = items;
    }

    public List<PairResponse> getItems() {
        return items;
    }

    public void setItems(List<PairResponse> items) {
        this.items = items;
    }
}
