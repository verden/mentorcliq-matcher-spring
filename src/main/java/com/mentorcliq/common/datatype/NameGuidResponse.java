package com.mentorcliq.common.datatype;

/**
 * @author William Arustamyan
 */

public final class NameGuidResponse {

    private String guid;

    private String name;

    public NameGuidResponse() {
    }

    public NameGuidResponse(String guid, String name) {
        this.guid = guid;
        this.name = name;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
