package com.mentorcliq.common.datatype;

/**
 * @author William Arustamyan
 */

public final class Score implements Comparable<Score> {

    private final String scoreValue;

    public Score(final String value) {
        this.scoreValue = value;
    }

    public Score(Object scoreValue) {
        this(scoreValue.toString());
    }

    public Score(int value) {
        this.scoreValue = Integer.toString(value);
    }

    public Score(double value) {
        this.scoreValue = Double.toString(value);
    }

    public int asInteger() {
        return Integer.parseInt(this.scoreValue);
    }

    public double asDouble() {
        return Double.parseDouble(this.scoreValue);
    }

    public String scoreValue() {
        return this.scoreValue;
    }

    public String asPercentage() {
        return this.scoreValue + "%";
    }

    @Override
    public int compareTo(Score o) {
        return Double.compare(this.asDouble(), o.asDouble());
    }
}
