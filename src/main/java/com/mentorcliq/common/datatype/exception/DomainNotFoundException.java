package com.mentorcliq.common.datatype.exception;

/**
 * @author William Arustamyan
 */

public class DomainNotFoundException extends RuntimeException {

    public DomainNotFoundException(String message) {
        super(message);
    }
}
