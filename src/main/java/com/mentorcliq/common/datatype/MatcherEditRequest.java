package com.mentorcliq.common.datatype;

/**
 * @author William Arustamyan
 */


public final class MatcherEditRequest {

    private String guid;

    private String name;

    private String description;

    private String expression;

    private String trueCaseValue;

    private String falseCaseValue;

    public MatcherEditRequest() {
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getTrueCaseValue() {
        return trueCaseValue;
    }

    public void setTrueCaseValue(String trueCaseValue) {
        this.trueCaseValue = trueCaseValue;
    }

    public String getFalseCaseValue() {
        return falseCaseValue;
    }

    public void setFalseCaseValue(String falseCaseValue) {
        this.falseCaseValue = falseCaseValue;
    }
}
