package com.mentorcliq.common.datatype;

import com.mentorcliq.common.AbstractApiResponse;
import com.mentorcliq.entity.Employee;
import com.mentorcliq.entity.Matcher;

import java.util.ArrayList;
import java.util.List;

/**
 * @author William Arustamyan
 */

public final class MatcherEmployeeCombinedResponse extends AbstractApiResponse {

    private List<NameGuidResponse> employees;
    private List<NameGuidResponse> matchers;

    public MatcherEmployeeCombinedResponse() {

    }

    public MatcherEmployeeCombinedResponse(final List<Employee> employees, final List<Matcher> matchers) {
        this.employees = new ArrayList<>(employees.size());
        this.matchers = new ArrayList<>(matchers.size());
        employees.forEach(it -> this.employees.add(new NameGuidResponse(it.getGuid().toString(), it.getName())));
        matchers.forEach(it -> this.matchers.add(new NameGuidResponse(it.getGuid().toString(), it.getName())));
    }

    public List<NameGuidResponse> getEmployees() {
        return employees;
    }

    public void setEmployees(List<NameGuidResponse> employees) {
        this.employees = employees;
    }

    public List<NameGuidResponse> getMatchers() {
        return matchers;
    }

    public void setMatchers(List<NameGuidResponse> matchers) {
        this.matchers = matchers;
    }
}
