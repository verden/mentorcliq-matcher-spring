package com.mentorcliq.common;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author William Arustamyan
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractApiResponse {

    private Long timeSpent;

    private String apiVersion;

    public AbstractApiResponse() {
        super();
    }

    public Long getTimeSpent() {
        return timeSpent;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setTimeSpent(Long timeSpent) {
        this.timeSpent = timeSpent;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }
}