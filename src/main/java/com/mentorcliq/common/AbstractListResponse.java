package com.mentorcliq.common;

import java.util.ArrayList;
import java.util.List;

/**
 * @author William Arustamyan
 */
public final class AbstractListResponse<T> extends AbstractApiResponse {

    private long totalItems;

    private List<T> items = new ArrayList<>();

    public AbstractListResponse() {
        super();
    }

    public AbstractListResponse(List<T> items) {
        this.totalItems = items.size();
        this.items = items;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
