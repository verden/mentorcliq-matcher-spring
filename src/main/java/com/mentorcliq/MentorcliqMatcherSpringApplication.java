package com.mentorcliq;

import com.mentorcliq.startup.StartupRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class MentorcliqMatcherSpringApplication implements CommandLineRunner {

    private StartupRunner startupRunner;

    @Override
    public void run(String... args) {
        this.startupRunner.onStart();
    }

    public static void main(String[] args) {
        new SpringApplicationBuilder(MentorcliqMatcherSpringApplication.class)
                .bannerMode(Banner.Mode.OFF)
                .run(args);
    }


    @Autowired
    public void setSetupRunner(final StartupRunner runner) {
        this.startupRunner = runner;
    }
}
