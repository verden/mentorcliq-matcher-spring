package com.mentorcliq.resource;

import com.mentorcliq.common.AbstractListResponse;
import com.mentorcliq.entity.Employee;
import com.mentorcliq.service.employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author William Arustamyan
 */


@RestController
@RequestMapping("employee")
public class EmployeeEndpoint {

    private EmployeeService employeeService;


    @PostMapping("/upload")
    public AbstractListResponse<Employee> employeeFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
        return new AbstractListResponse<>(this.employeeService.upload(file.getInputStream()));
    }

    @GetMapping("/items")
    public AbstractListResponse<Employee> list() {
        return new AbstractListResponse<>(this.employeeService.list());
    }

    @Autowired
    public void setEmployeeService(final EmployeeService service) {
        this.employeeService = service;
    }
}
