package com.mentorcliq.resource;

import com.mentorcliq.common.AbstractListResponse;
import com.mentorcliq.common.datatype.MatcherAddRequest;
import com.mentorcliq.common.datatype.MatcherEditRequest;
import com.mentorcliq.entity.Matcher;
import com.mentorcliq.service.matcher.MatcherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author William Arustamyan
 */


@RestController
@RequestMapping("matcher")
public class MatcherEndpoint {


    private MatcherService matcherService;


    @GetMapping("/items")
    public AbstractListResponse<Matcher> list() {
        return new AbstractListResponse<>(this.matcherService.list());
    }

    @PutMapping("/edit")
    public AbstractListResponse<Matcher> edit(@RequestBody MatcherEditRequest editRequest) {
        return new AbstractListResponse<>(matcherService.edit(editRequest));
    }

    @PostMapping("/add")
    public AbstractListResponse<Matcher> add(@RequestBody MatcherAddRequest addRequest) {
        return new AbstractListResponse<>(matcherService.add(addRequest));
    }

    @DeleteMapping(value = "/remove/{guid}")
    public AbstractListResponse<Matcher> delete(@PathVariable String guid) {
        return new AbstractListResponse<>(this.matcherService.delete(UUID.fromString(guid)));
    }

    @Autowired
    public void setMatcherService(MatcherService matcherService) {
        this.matcherService = matcherService;
    }
}
