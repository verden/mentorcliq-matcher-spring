package com.mentorcliq.resource;

import com.mentorcliq.common.datatype.MatchRequest;
import com.mentorcliq.common.datatype.MatcherEmployeeCombinedResponse;
import com.mentorcliq.common.datatype.MatchingResultResponse;
import com.mentorcliq.common.datatype.RecommendationResponse;
import com.mentorcliq.service.employee.EmployeeService;
import com.mentorcliq.service.matcher.MatcherService;
import com.mentorcliq.service.matching.MatchingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author William Arustamyan
 */


@RestController
@RequestMapping("matching")
public class MatchingEndpoint {

    private EmployeeService employeeService;

    private MatcherService matcherService;

    private MatchingService matchingService;


    @GetMapping("/items")
    public MatcherEmployeeCombinedResponse employeeMatcherItems() {
        return new MatcherEmployeeCombinedResponse(this.employeeService.list(), this.matcherService.list());
    }


    @PostMapping("/match")
    public MatchingResultResponse match(@RequestBody MatchRequest matchRequest) {
        return this.matchingService.doMatch(matchRequest);
    }

    @GetMapping("/recommendations")
    public RecommendationResponse recommendations() {
        return this.matchingService.recommendations();
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Autowired
    public void setMatcherService(MatcherService matcherService) {
        this.matcherService = matcherService;
    }

    @Autowired
    public void setMatchingService(MatchingService matchingService) {
        this.matchingService = matchingService;
    }
}
