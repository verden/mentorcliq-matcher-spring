package com.mentorcliq.parser;

import com.mentorcliq.entity.Employee;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author William Arustamyan
 */

@Service
public final class EmployeeFileParser implements UploadFileParser<List<Employee>> {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeFileParser.class);

    @Override
    public List<Employee> parse(final InputStream stream) {
        final Reader reader = new InputStreamReader(stream);
        Iterable<CSVRecord> records;
        try {
            records = CSVFormat.EXCEL.withHeader().parse(reader);
        } catch (IOException e) {
            logger.error("Unable to parse file ... ", e);
            return Collections.emptyList();
        }
        return convertToEmployees(records);
    }

    private List<Employee> convertToEmployees(final Iterable<CSVRecord> records) {
        final List<Employee> employees = new ArrayList<>();
        for (final CSVRecord r : records) {
            employees.add(new Employee(
                    r.get("Name"),
                    r.get("Email"),
                    r.get("Division"),
                    Integer.parseInt(r.get("Age")),
                    Integer.parseInt(r.get("Timezone"))
            ));
        }
        return employees;
    }
}
