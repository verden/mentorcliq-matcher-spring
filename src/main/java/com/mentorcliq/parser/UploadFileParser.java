package com.mentorcliq.parser;

import java.io.InputStream;

/**
 * @author William Arustamyan
 */

public interface UploadFileParser<T> {

    T parse(InputStream stream);
}
