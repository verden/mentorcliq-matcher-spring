package com.mentorcliq.repository;

import com.mentorcliq.entity.Matcher;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

public interface MatcherRepository extends CrudRepository<Matcher, UUID> {

    List<Matcher> findAll();

}
