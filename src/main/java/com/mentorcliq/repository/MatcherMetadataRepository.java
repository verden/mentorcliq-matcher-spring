package com.mentorcliq.repository;

import com.mentorcliq.entity.MatcherMetadata;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

/**
 * @author William Arustamyan
 */

public interface MatcherMetadataRepository extends CrudRepository<MatcherMetadata, UUID> {

    MatcherMetadata findByMatchableType(String type);
}
