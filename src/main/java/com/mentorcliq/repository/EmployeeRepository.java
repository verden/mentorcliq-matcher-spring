package com.mentorcliq.repository;

import com.mentorcliq.entity.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

public interface EmployeeRepository extends CrudRepository<Employee, UUID> {
    List<Employee> findAll();
}
