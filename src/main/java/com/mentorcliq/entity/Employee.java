package com.mentorcliq.entity;

import com.mentorcliq.common.datatype.annotations.Matchable;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * @author William Arustamyan
 */


@Matchable(type = "employee")
@Entity
@Table(name = "t_employees")
public class Employee {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "guid", updatable = false, nullable = false)
    private UUID guid;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "division")
    private String division;

    @Column(name = "age")
    private int age;

    @Column(name = "timezone")
    private int timeZone;


    public Employee(String name, String email, String division, int age, int timeZone) {
        this.name = name;
        this.email = email;
        this.division = division;
        this.age = age;
        this.timeZone = timeZone;
    }

    public Employee() {
    }

    public UUID getGuid() {
        return guid;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(int timeZone) {
        this.timeZone = timeZone;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "guid=" + guid +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", division='" + division + '\'' +
                ", age=" + age +
                ", timeZone=" + timeZone +
                '}';
    }
}
