package com.mentorcliq.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * @author William Arustamyan
 */

@Entity
@Table(name = "t_matchers")
public class Matcher {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "guid", updatable = false, nullable = false)
    private UUID guid;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "expression")
    private String expression;

    @Column(name = "true_case_val")
    private String trueCaseValue;

    @Column(name = "false_case_val")
    private String falseCaseValue;

    @Column(name = "matcher_type")
    private String matcherType;


    public Matcher() {
    }

    public Matcher(String matcherType) {
        this.matcherType = matcherType;
    }

    public UUID getGuid() {
        return guid;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getTrueCaseValue() {
        return trueCaseValue;
    }

    public void setTrueCaseValue(String trueCaseValue) {
        this.trueCaseValue = trueCaseValue;
    }

    public String getFalseCaseValue() {
        return falseCaseValue;
    }

    public void setFalseCaseValue(String falseCaseValue) {
        this.falseCaseValue = falseCaseValue;
    }

    public String getMatcherType() {
        return matcherType;
    }

    public void setMatcherType(String matcherType) {
        this.matcherType = matcherType;
    }
}
