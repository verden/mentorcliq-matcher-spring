package com.mentorcliq.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * @author William Arustamyan
 */


@Entity
@Table(name = "t_matcher_metadata")
public class MatcherMetadata {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "guid", updatable = false, nullable = false)
    private UUID guid;

    @Column(name = "matchable_type", unique = true)
    private String matchableType;

    public MatcherMetadata(String matchableType) {
        this.matchableType = matchableType;
    }

    public MatcherMetadata() {
    }

    public UUID getGuid() {
        return guid;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public String getMatchableType() {
        return matchableType;
    }

    public void setMatchableType(String matchableType) {
        this.matchableType = matchableType;
    }
}
